package com.msinus.msinus;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class Information extends Activity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        Intent intent = getIntent();
        int position = intent.getIntExtra("position", 0);
        String clickedname = intent.getStringExtra("name");

        TextView ufees = findViewById(R.id.ufees);
        TextView urank = findViewById(R.id.urank);
        TextView uname = findViewById(R.id.uname);
        TextView ugre = findViewById(R.id.ugre);
        TextView utoefl = findViewById(R.id.utoefl);

        TextView ustate = findViewById(R.id.ustate);

        DatabaseHelper myDatabaseHelper = new DatabaseHelper(Information.this);
        myDatabaseHelper.openDataBase();

        String[] name = myDatabaseHelper.getUniversityName(clickedname);
        Log.i("Names", name[0]);
        String state = myDatabaseHelper.getState(clickedname);


        myDatabaseHelper.close();
        uname.setText(" " +name[0]);
        ufees.setText(" " +name[1]+"L");
        ugre.setText(" " +name[2]);
        utoefl.setText(" " +name[3]);
        urank.setText(" #" +name[4]);

        ustate.setText(" " +state);
    }
}
