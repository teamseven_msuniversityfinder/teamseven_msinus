package com.msinus.msinus;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;


/**
 * Created by Osban on 04-03-2018.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder>{

    private static ArrayList<university_model> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView word,rank;
        private Context context;


        public MyViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            this.word= itemView.findViewById(R.id.name);
            this.rank = itemView.findViewById(R.id.rank);
            context = itemView.getContext();

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    String name = (String) word.getText();

                    Intent myIntent = new Intent(context, Information.class);
                    myIntent.putExtra("position", pos);
                    myIntent.putExtra("name", name);
                    context.startActivity(myIntent);
                }
            });

        }

        @Override
        public void onClick(View v) {
        }

    }

    public CustomAdapter(ArrayList<university_model> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);


        final MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView name= holder.word;
        TextView rank = holder.rank;

        name.setText(dataSet.get(listPosition).getWord());
        rank.setText(dataSet.get(listPosition).getMeaning());

    }

    public void setFilter(ArrayList<university_model> newList){
        dataSet = new ArrayList<>();
        dataSet.addAll(newList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}