package com.msinus.msinus;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    final String[] filters = new String[]{"Ascending", "Descending","Ranking"};
    final int pos= -1;
    int selectedPosition = 5;
    int filterActive = 0;
    int checkFilled = 0;
    String fees,gre_score;

    private static RecyclerView.Adapter adapter;
    CustomAdapter searchadapter=null;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    public static ArrayList<university_model> data;
    DatabaseHelper db ;
    ArrayList<String> universitylist;
    ArrayList<String> statelist;
    ArrayList<String> ranklist;
    LinkedHashMap<String,String> namelist;
    LinkedHashMap<String,String> statelisthash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        db= new DatabaseHelper(this);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        fetchData();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void fetchData()
    {
        data = new ArrayList<university_model>();
        db =new DatabaseHelper(this);
        try {

            db.createDataBase();
            db.openDataBase();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        namelist=new LinkedHashMap<>();
        statelisthash=new LinkedHashMap<>();
        int ii,kk;
        Cursor cursor = query();
        Cursor cursor1 = query1();
        ii=cursor.getColumnIndex("name");
        kk=cursor1.getColumnIndex("state_name");
        universitylist=new ArrayList<String>();
        statelist = new ArrayList<String>();


        ranklist= new ArrayList<String>();
        while (cursor.moveToNext()){
            namelist.put(cursor.getString(ii), cursor.getString(cursor.getColumnIndex("rank")));
        }
        Iterator entries = namelist.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            universitylist.add(String.valueOf(thisEntry.getKey()));
            ranklist.add("# "+String.valueOf(thisEntry.getValue()));
        }

        while (cursor1.moveToNext()){
            statelisthash.put(cursor1.getString(kk), cursor1.getString(cursor1.getColumnIndex("state_name")));
        }
        Iterator entries1 = statelisthash.entrySet().iterator();
        while (entries1.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries1.next();
            statelist.add(String.valueOf(thisEntry.getKey()));
        }


        Log.i("State List:", String.valueOf(statelist));


        for (int i = 0; i < universitylist.size(); i++) {
            data.add(new university_model(universitylist.get(i), ranklist.get(i)));
        }
        Log.i("Names1", String.valueOf(universitylist));
        adapter = new CustomAdapter(data);
        recyclerView.setAdapter(adapter);
    }
    public Cursor query1(){
        SQLiteDatabase sd = db.getReadableDatabase();
        Cursor cursor1;
        cursor1 = sd.rawQuery("SELECT state_name FROM states", new String[0]);

        return cursor1;
    }

    public Cursor query(){
        SQLiteDatabase sd = db.getReadableDatabase();

        Cursor cursor;

        if (((selectedPosition == 0) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 1) || (checkFilled == 4))){
            cursor = sd.rawQuery("SELECT * FROM universities WHERE fees <?  ORDER BY name ASC", new String[] {fees + ""});
            return cursor;
        }else if (((selectedPosition == 1) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 1) || (checkFilled == 4))) {
            cursor = sd.rawQuery("SELECT * FROM universities WHERE fees <?  ORDER BY name DESC", new String[] {fees + ""});
            return cursor;
        }else if (((selectedPosition == 2) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 1) || (checkFilled == 4))) {
            cursor = sd.rawQuery("SELECT * FROM universities WHERE fees <?  ORDER BY rank ASC", new String[] {fees + ""});
            return cursor;
        }else if (((selectedPosition == 0) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 2) || (checkFilled == 4))) {
            cursor = sd.rawQuery("SELECT * FROM universities WHERE gre_score <? ORDER BY name ASC", new String[] {gre_score + ""});
            return cursor;
        }else if (((selectedPosition == 1) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 2) || (checkFilled == 4))) {
            cursor = sd.rawQuery("SELECT * FROM universities WHERE gre_score <? ORDER BY name DESC", new String[] {gre_score + ""});
            return cursor;
        }else if (((selectedPosition == 2) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 2) || (checkFilled == 4))) {
            cursor = sd.rawQuery("SELECT * FROM universities WHERE gre_score <? ORDER BY rank ASC", new String[] {gre_score + ""});
            return cursor;
        }
        else if (((selectedPosition == 0) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 3) || (checkFilled == 4))) {
            cursor = sd.rawQuery("SELECT * FROM universities WHERE fees <? AND gre_score <? ORDER BY name ASC", new String[] {fees,gre_score + ""});
            return cursor;
        }else if (((selectedPosition == 1) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 3) || (checkFilled == 4))) {
            cursor = sd.rawQuery("SELECT * FROM universities WHERE fees <? AND gre_score <? ORDER BY name DESC", new String[] {fees,gre_score + ""});
            return cursor;
        }else if (((selectedPosition == 2) || (selectedPosition == -1)) && filterActive == 1 && ((checkFilled == 3) || (checkFilled == 4))) {
            cursor = sd.rawQuery("SELECT * FROM universities WHERE fees <? AND gre_score <? ORDER BY rank ASC", new String[] {fees,gre_score + ""});
            return cursor;
        }else{
            cursor = sd.rawQuery("SELECT * FROM universities", new String[0]);
            return cursor;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem search = (menu.findItem(R.id.menuSearch));
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String newText) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<university_model> newList = new ArrayList<>();

        if (newText.equals("")){
            searchadapter = new CustomAdapter(data);
            recyclerView.setAdapter(searchadapter);
        }
        for (university_model word : data) {

            if (word.getWord().toLowerCase().contains(newText)) {
                newList.add(word);
            }
        }


        if (newList.isEmpty() == false) {
            searchadapter = new CustomAdapter(newList);
            searchadapter.setFilter(newList);
            recyclerView.setAdapter(searchadapter);
            newList.clear();
        }
        else {
            searchadapter = new CustomAdapter(newList);
            searchadapter.setFilter(newList);
            recyclerView.setAdapter(searchadapter);
            Toast.makeText(getApplicationContext(), "No Matches Found", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_exit) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_universities) {
            adapter = new CustomAdapter(data);
            recyclerView.setAdapter(adapter);

        } else if (id == R.id.nav_filter) {

            //https://stackoverflow.com/questions/10903754/input-text-dialog-android

            final View filter_input = getLayoutInflater().inflate(R.layout.filters, null);
            final EditText getFees = filter_input.findViewById(R.id.getFees);
            final EditText getGre = filter_input.findViewById(R.id.getGre);

            //https://stackoverflow.com/questions/5660887/how-to-select-a-entry-in-alertdialog-with-single-choice-checkbox-android

            AlertDialog ad = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Filter")
                    .setView(filter_input)
                    .setSingleChoiceItems(filters, pos,null)
                    .setPositiveButton( "Apply", new DialogInterface.OnClickListener() {
                        public void onClick( DialogInterface dialog, int whichButton)
                        {
                            try {
                                selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                                filterActive = 1;
                                fees = getFees.getText().toString();
                                gre_score = getGre.getText().toString();

                                if (fees.isEmpty()==false && gre_score.isEmpty()==true){
                                    checkFilled = 1;
                                }else if (fees.isEmpty()==true && gre_score.isEmpty()==false){
                                    checkFilled = 2;
                                }else if (fees.isEmpty()==false && gre_score.isEmpty()==false){
                                    checkFilled = 3;
                                }else{
                                    checkFilled = 4;
                                }
                                dialog.dismiss();
                                fetchData();
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(),"No filters selected",Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    })
                    .setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
                        public void onClick( DialogInterface dialog, int whichButton)
                        {
                           dialog.dismiss();
                        }
                    })
                    .create();
            ad.show();

        } else if (id == R.id.nav_feedback) {

        } else if (id == R.id.nav_manage) {

            final View filter_input = getLayoutInflater().inflate(R.layout.login, null);
            final EditText getusername = filter_input.findViewById(R.id.username);
            final EditText getpassword = filter_input.findViewById(R.id.password);
            AlertDialog ad = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Admin Login")
                    .setView(filter_input)
                    .setPositiveButton( "Login", new DialogInterface.OnClickListener() {
                        public void onClick( DialogInterface dialog, int whichButton)
                        {
                            try {
                                String username = getusername.getText().toString();
                                String password = getpassword.getText().toString();
                                Toast.makeText(getApplicationContext(),""+username,Toast.LENGTH_SHORT).show();
                                if (username.equals("osban") && password.equals("qwerty")){
                                    startActivity(new Intent(getApplicationContext(),Admin.class));
                                    finish();
                                    //dialog.dismiss();
                                }
                                else{
                                    Toast.makeText(getApplicationContext(),"Incorrect login",Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(),"Error in login",Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    })
                    .setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
                        public void onClick( DialogInterface dialog, int whichButton)
                        {
                            dialog.dismiss();
                        }
                    })
                    .create();
            ad.show();


        } else if (id == R.id.nav_suggest) {
            Intent i = new Intent(getApplicationContext(),enterScore.class);
            i.putExtra("States", statelist);
            startActivity(i);
        } else if (id == R.id.nav_share) {

            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Hey I found this new App which cleared all my views towards foreign studies. Try it !! ";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Download now");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        } else if (id == R.id.nav_contact) {

            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "msuniversitiesinfo@gmail.com"));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "");

            startActivity(Intent.createChooser(emailIntent, "Send Email to..."));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
