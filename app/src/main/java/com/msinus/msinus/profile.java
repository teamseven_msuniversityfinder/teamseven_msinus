package com.msinus.msinus;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class profile extends Activity{

    private static RecyclerView.Adapter adapter;
    CustomAdapter searchadapter=null;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView profile_recyclerView;
    DatabaseHelper db;
    public static ArrayList<university_model> data;
    ArrayList<String> universitylist;
    ArrayList<String> ranklist;
    LinkedHashMap<String,String> namelist;
    String gre1,toefl1,percent1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        profile_recyclerView = findViewById(R.id.profile_recyclerview);
        db= new DatabaseHelper(this);
        layoutManager = new LinearLayoutManager(this);
        profile_recyclerView.setLayoutManager(layoutManager);
        profile_recyclerView.setItemAnimator(new DefaultItemAnimator());



        Intent intent = getIntent();
        gre1 = intent.getExtras().getString("pgre");
        toefl1 = intent.getExtras().getString("ptoefl");
        percent1 = intent.getExtras().getString("ppercent");

        TextView pgre = findViewById(R.id.pgre);
        TextView ptoefl = findViewById(R.id.ptoefl);
        TextView ppercent = findViewById(R.id.ppercentage);


        pgre.setText(""+gre1);
        ptoefl.setText(""+toefl1);
        ppercent.setText(""+percent1);

        Float gre = Float.parseFloat(pgre.getText().toString().trim());
        Float gpa = Float.parseFloat(ppercent.getText().toString().trim());

        int n1,n2;
        if(gre > 330){
            n1 = 4;
        }else if(gre > 320){
            n1 = 3;
        }else if(gre > 310){
            n1 = 2;
        }else{
            n1 = 1;
        }
        if(gpa > 9){
            n2 = 4;
        }else if(gpa > 8){
            n2 = 3;
        }else if(gpa > 7){
            n2 = 2;
        }else{
            n2 = 1;
        }

        int student_rank = n1 * n2;
        int result = (int) (-7.2234 * student_rank + 110.088);

        Toast.makeText(getApplicationContext(),""+result,Toast.LENGTH_SHORT).show();
        fetchData();

    }

    public void fetchData()
    {
        data = new ArrayList<university_model>();
        db =new DatabaseHelper(this);
        try {

            db.createDataBase();
            db.openDataBase();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        namelist=new LinkedHashMap<>();
        int ii;
        Cursor cursor = query();
        ii=cursor.getColumnIndex("name");
        universitylist=new ArrayList<String>();

        ranklist= new ArrayList<String>();
        while (cursor.moveToNext()){
            namelist.put(cursor.getString(ii), cursor.getString(cursor.getColumnIndex("rank")));
        }
        Iterator entries = namelist.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            universitylist.add(String.valueOf(thisEntry.getKey()));
            ranklist.add("# "+String.valueOf(thisEntry.getValue()));
        }

        for (int i = 0; i < universitylist.size(); i++) {
            data.add(new university_model(universitylist.get(i), ranklist.get(i)));
        }
        //Log.i("Names1", String.valueOf(universitylist));
        adapter = new CustomAdapter(data);
        profile_recyclerView.setAdapter(adapter);
    }

    public Cursor query(){
        SQLiteDatabase sd = db.getReadableDatabase();

        Cursor cursor;
        cursor = sd.rawQuery("SELECT * FROM universities WHERE gre_score <= ? AND toefl_score <= ? ORDER BY rank ASC", new String[]{gre1,toefl1});
        return cursor;

    }

}
