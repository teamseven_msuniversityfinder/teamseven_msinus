package com.msinus.msinus;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class enterScore extends Activity {

    private static final String[] COUNTRIES = new String[] {
            "United States", "Canada", "Australia", "Germany", "Spain"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_score);

        final Spinner spinner =  findViewById(R.id.s1);
        final Spinner spinner2 =  findViewById(R.id.s2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.budgetfrom, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.budgetto, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter1);

        ArrayList<String> states;

        Bundle b = getIntent().getExtras();
        states = b.getStringArrayList("States");

      ArrayAdapter<String> statesadapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,states );
        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.a1);
        textView.setAdapter(statesadapter);

        @SuppressLint("ResourceType") ArrayAdapter<String> countryadapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, COUNTRIES );
        AutoCompleteTextView textView1 = (AutoCompleteTextView)
                findViewById(R.id.a2);
        textView1.setAdapter(countryadapter);

        final EditText e1 = findViewById(R.id.e1);
        final EditText e2 = findViewById(R.id.e2);
        final EditText e3 = findViewById(R.id.e3);
        final EditText a2 = findViewById(R.id.a2);
        final EditText a1 = findViewById(R.id.a1);


        Button b1 = findViewById(R.id.b1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int s1 = Integer.parseInt(spinner.getSelectedItem().toString());
                int s2 = Integer.parseInt(spinner2.getSelectedItem().toString());

                String pgre= e1.getText().toString();
                String ptoefl= e2.getText().toString();
                String ppercent= e3.getText().toString();
                String pcountry= a2.getText().toString();
                String pstate= a1.getText().toString();

                if (s1 > s2) {
                    Toast.makeText(getApplicationContext(), "Invalid Range", Toast.LENGTH_SHORT).show();
                }else if(pgre.isEmpty() || ptoefl.isEmpty() || ppercent.isEmpty() || pcountry.isEmpty() || pstate.isEmpty()){
                    Toast.makeText(getApplicationContext(),"All fields are mandatory",Toast.LENGTH_SHORT).show();
                }else{

                    Intent i = new Intent(getApplicationContext(),profile.class);
                    i.putExtra("pgre",pgre);
                    i.putExtra("ptoefl",ptoefl);
                    i.putExtra("ppercent",ppercent);
                    i.putExtra("pcountry",pcountry);
                    i.putExtra("pstate",pstate);
                    i.putExtra("from",s1);
                    i.putExtra("to",s2);

                    startActivity(i);
                }
            }
        });

    }
}
