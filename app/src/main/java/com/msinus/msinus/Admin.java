package com.msinus.msinus;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Admin extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        final EditText uname = findViewById(R.id.uname);
        final EditText ufees = findViewById(R.id.ufees);
        final EditText urank = findViewById(R.id.urank);
        final EditText ugre = findViewById(R.id.ugre);
        final EditText utoefl = findViewById(R.id.utoefl);

        final Spinner spinner = (Spinner) findViewById(R.id.states);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.states, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        final Button add = findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name= uname.getText().toString();
                String fees= ufees.getText().toString();
                String rank= urank.getText().toString();
                String gre= ugre.getText().toString();
                String toefl= utoefl.getText().toString();
                int state= spinner.getSelectedItemPosition();
                if (name.isEmpty()==true){
                    Toast.makeText(getApplicationContext(),"Please enter a university name",Toast.LENGTH_SHORT).show();
                }
                else{
                    insertData(name,fees,rank,gre,toefl,state);
                    Toast.makeText(getApplicationContext(),"University added to list",Toast.LENGTH_SHORT).show();
                }
            }
        });

        final Button delete = findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name= uname.getText().toString();
                String rank= urank.getText().toString();
                if (rank.isEmpty()) {
                    deleteData(name);
                    Toast.makeText(getApplicationContext(), "University "+name+"was removed", Toast.LENGTH_SHORT).show();
                }
                else{
                    deleteData(rank);
                    Toast.makeText(getApplicationContext(), "University with rank "+rank+" was removed", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void deleteData(String name) {
        String d="name";
        DatabaseHelper helper = new DatabaseHelper(this);
        SQLiteDatabase database = helper.getWritableDatabase();
        database.delete("universities",d + " = ?", new String[] { name });
        //database.rawQuery("DELETE FROM universities WHERE name=?",new String[] {name + ""} );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }

    private void insertData(String name,String fees,String rank,String gre,String toefl,Integer state) {
        DatabaseHelper helper = new DatabaseHelper(this);
        SQLiteDatabase database = helper.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("name", name);
        content.put("fees", fees);
        content.put("rank", rank);
        content.put("gre_score", gre);
        content.put("toefl_score", toefl);
        content.put("state_id", state);

        database.insert("universities", null, content);

    }
}
